using haystack
using connExt

**
** bfitSmapModel
**
@Js
const class BfitSmapModel : ConnModel
{
  new make() : super(BfitSmapModel#.pod)
  {
    connProto = Etc.makeDict([
      "dis": "sMap Connector",
      "uri": `https://trendr.buildingrobotics.com/backend/`,
      "username": "smap"
    ])
    name = "bfitSmap"
    learnFunc = "bfitSmapLearn"
    pingFunc = "bfitSmapPing"
    syncHisFunc = "bfitSmapSyncHis"
    syncCurFunc = "bfitSmapSyncCur"
    connTag = "bfitSmapConn"
    connRefTag = "bfitSmapConnRef"
    pointHisTag = "bfitSmapHis"
  }

  override const Dict connProto
  override Type? pointAddrType() { Str# }
  override Bool isPollingSupported()  { true }
  override Bool isCurSupported()      { true }
  override Bool isHisSupported()      { true }
  override Bool isLearnSupported()    { true }
  override PollingMode pollingMode() { PollingMode.buckets }
}
