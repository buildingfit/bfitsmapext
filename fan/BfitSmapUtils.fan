using axon
using web
using haystack
using util
using connExt
using skyarcd

class BfitSmapUtils
{ 
  ** Sends a simple get request to the server. Errors if anything other than 200 is returned.
  static Bool ping(Uri path, Str key){
    WebClient client := WebClient("$path?key=$key".toUri())
    client.reqMethod = "GET"

    //Get response
    client.writeReq
    client.readRes

    if(client.resCode != 200) throw Err("Client error, response code: $client.resCode")
    return true // If we get this far, it was successful.
  }
  
  
  ** Queries the sMAP server for the input uuid, and returns the response.
  static Dict readUuid(Uri path, Str key, Str uuid){
    //create request
    Str get := "api/tags/uuid/$uuid?key=$key"
    WebClient client := WebClient(path + get.toUri)
    client.reqMethod = "GET"
    
    //Get response
    client.writeReq
    client.readRes
    if(client.resCode != 200) echo("Client error, response code: $client.resCode")
    
    //Format Data
    [Obj:Obj] json := ((Obj[])(JsonInStream(client.resIn).readJson)).first
    [Str:Str] map := flattenMap(json)
    return Etc.makeDict(map)
  }
  
  
  static Grid queryLearn(Dict connRec, Str key, Str? type){
    Log log := Log.get("bfitSmap")
    Ref ref := connRec.id
    Uri path := connRec["uri"]
    
    //create request
    Str temp := ""
    if(type != null) temp = "/" + type
    Bool typeIsTags := type != null && type.contains("/")
    Str get := "api/"
    if(typeIsTags) get = get+"tags" // type is either tags or a query.
    else get = get+"query"
    get = get+"$temp?key=$key"
    WebClient client := WebClient(path + get.toUri)
    client.reqMethod = "GET"
    
    //Get response
    client.writeReq
    client.readRes
    if(client.resCode != 200) echo("Client error, response code: $client.resCode")
    [Str:Obj][] maps := [,]
    
    //Learn more
    if(!typeIsTags){
      try{
        Str[] list := JsonInStream(client.resIn).readJson
        list.each(|Str str|{
          str = str.replace("/", "__") // Avoid any slashes within names.
          Str learn := str
          if(type != null) learn = "$type/$str"
          maps.add(["dis":str, "learn": learn])
        })
      }
      catch (Err err){ log.info(client.reqUri.toStr, err)}
    }
    
    //List of points
    else{
      //Format Data
      [Obj:Obj][] json := (JsonInStream(client.resIn).readJson)
      json.each(|Obj:Obj map|{
        [Str:Obj] point := [:]
        [Str:Str?] flat := flattenMap(map)
        flat = flat.map(|Str? val -> Str?|{ // Make any empty strings into null.
          if(val == "") return null 
          else return val
        })
        point.set("point", Marker.val)
        point.set("bfitSmapConnRef", ref)
        point.set("bfitSmapCur",flat["uuid"])
        point.set("bfitSmapHis",flat["uuid"])
        if(flat["description"] != null) point.set("description",flat["description"])
        if(flat["deviceDescription"] != null) point.set("deviceDescription",flat["deviceDescription"])
        if(flat["deviceName"] != null) point.set("sourceEquipName",flat["deviceName"])
        if(flat["pointName"] != null) point.set("sourcePointName",flat["pointName"])
        if(flat["path"] != null) point.set("sourcePointPath",flat["path"])
        if(flat["vendor__name"] != null) point.set("vendorName",flat["vendor__name"])
        
        if(flat["pointName"] != null) point.set("dis",flat["pointName"]) // Set display name
        else if(flat["name"] != null) point.set("dis",flat["name"])
        else if(flat["description"] != null) point.set("dis",flat["description"])
        else point.set("dis",flat["uuid"])
        
        [Str:Str] readingTypes := ["double":"Number","long":"Number"] // Extract the kind from the 'readingType'.
        Str? kind := null
        if(flat["readingType"] != null) {
          point.set("readingType",flat["readingType"])
          kind = readingTypes[flat["readingType"]]
        }
        if(kind != null) point.set("kind", kind)
        
        Unit? unit := null
        if(flat["unitofMeasure"] != null) {
          point.set("unitofMeasure",flat["unitofMeasure"])
          unit = Unit.fromStr(flat["unitofMeasure"], false)
        }
        if(unit != null) point.set("unit", unit.toStr())
        
        TimeZone? timezone := null
        if(flat["timezone"] != null) timezone = TimeZone.fromStr(flat["timezone"], false)
        if(timezone != null) point.set("tz", timezone.toStr())
        
        maps.add(point)
      })
    }
    return Etc.makeMapsGrid(null,maps)
  }
  
  
  static Obj[] getData(BfitSmapConn connector, Dict point, Int[] dates){
    Log log := Log.get("bfitSmap")
    Str? uuid := point.get("bfitSmapHis") ?: point.get("bfitSmapCur")
    if(uuid == null) uuid = point.get("uuid")
    Ref connRef := point.get("bfitSmapConnRef")
    Uri path := connector.rec.get("uri")
    Str key := connector.proj().passwords().get(connector.id.toStr)
    //convert dates to unix time
    dates = dates.map|Int ticks -> Int| {DateTime.makeTicks(ticks).toJava}
    
    //create request
    Str get := "api/data/uuid/$uuid?key=$key&starttime=$dates.first&endtime=$dates.last"
    WebClient client := WebClient(path + get.toUri)
    client.reqMethod = "GET"
    //Get response
    client.writeReq
    client.readRes
    if(client.resCode != 200) echo("Client error, response code: $client.resCode")
    
    //Format Data
    [Obj:Obj]? json
    try{
      json = ((Obj[])(JsonInStream(client.resIn).readJson)).first
      json.get("Readings")
    } catch throw Err("No Data Available")
    data := json.get("Readings")
    
    if(data->first == null) return [,]
    //Change data back to nanosecond
    data = ((Obj[][])data).map|Obj[] pair -> Obj[]|{
      Int milli := 0
      if(pair.first.typeof == Str#){ milli =  Int.fromStr(pair.first)}
      else if(pair.first.typeof == Float# || pair.first.typeof == Decimal#) milli = Number.makeNum(pair.first).toInt
      else milli = pair.first
      Int nano := DateTime.fromJava(milli).ticks
      return [nano, pair.last]
    }
    return data
  }
  
  
  ** Flattens all items of a given map into a single map. That is, if there are any values of the input map that are
  ** themselves maps, this will bring pull their values upward into the main map.
  static Str:Obj flattenMap(Obj:Obj map){
    [Str:Obj] result := [:]
    map.each(|Obj val, Str key|{
      if(val is Map){result.setAll(flattenMap(val))}
      else if(val is Str){result.set(key.fromDisplayName,val)}
    })
    return result
  }
}