using haystack
using connExt
using web
using hisExt
using axon
using skyarcd

**
** Axon functions for bfitSmap
**
const class BfitSmapLib
{
  ** Pings the specified sMAP connector.
  @Axon
  static Str bfitSmapPing(Obj conn){
    Dict connRec := Context.cur.call("toRec",[conn])
    Uri path := connRec["uri"]
    Str key := Context.cur().proj().passwords().get(connRec.id.toStr)
    Bool success := BfitSmapUtils.ping(path, key)
    if(success) return "sMAP ping was successful"
    else return "sMAP ping failed"
  }
  
  ** Syncs current values to the specified points with sMAP connections.
  @Axon
  static Void bfitSmapSyncCur(Obj points){
    BfitSmapExt ext := Context.cur.ext("bfitSmap")
    ext.syncCur(points)
  }
  
  ** Recursively used to learn points from the builder app, can also be called from Folio. 
  ** 
  ** **Parameters**
  ** 
  ** - 'conn': The connector. Anything accepted by 'toRec' is a valid input.
  ** - 'learn': The Str representing the path to learn from. Pass in null to start at the root.
  ** 
  ** **Return**
  ** 
  ** A grid containing either points dictionaries or "learns" to drill down further into the system.
  @Axon
  static Grid bfitSmapLearn(Obj conn, Str? learn := null){
    Grid grid := Etc.makeEmptyGrid
    Dict connRec := Context.cur.call("toRec",[conn])
    Str key := Context.cur().proj().passwords.get(connRec.id.toStr)
    grid = BfitSmapUtils.queryLearn(connRec, key, learn)
    grid = grid.map(|Dict row -> Dict| {
      try{ return Etc.dictSet(row, "name", (row.get("name").toStr.decapitalize).replace(" ", "_")) }
      catch{ return row }
    })
    return grid
  }

  @NoDoc
  static Grid bfitSmapLearnEx(BfitSmapConn conn, Dict connRec, Str? learn := null){
    Grid grid := Etc.makeEmptyGrid
    
    
    Str key := conn.proj.passwords.get(connRec.id.toStr)
    grid = BfitSmapUtils.queryLearn(connRec, key, learn)
    grid = grid.map(|Dict row -> Dict| {
      try{ return Etc.dictSet(row, "name", (row.get("name").toStr.decapitalize).replace(" ", "_")) }
      catch{ return row }
    })
    return grid
  }
  
  ** Syncs historical values to the specified points with sMAP connections, for the specified date range.
  @Axon
  static Void bfitSmapSyncHis(Obj points, Obj? dates){
    BfitSmapExt ext := Context.cur.ext("bfitSmap")
    ext.syncHis(points, dates)
  }
}
