using haystack
using connExt
using skyarcd
using folio

**
** BfitSmap Extension
**
@ExtMeta
{
  name    = "bfitSmap"
  icon24  = `fan://bfitSmapExt/res/img/smap-icon-24.png` 
  icon72  = `fan://frescoRes/img/iconMissing72.png`
  depends = ["conn"]
}
const class BfitSmapExt : ConnImplExt
{
  @NoDoc new make( ) : super( BfitSmapModel()) {}
  
  override Void onStart(){
    // Convert old etc tags to bfit.
    migrateTagVal("etcSmapConn","bfitSmapConn",proj)
    migrateTagVal("etcSmapConnRef","bfitSmapConnRef",proj)
    migrateTagVal("etcSmapCur","bfitSmapCur",proj)
    migrateTagVal("etcSmapHis","bfitSmapHis",proj)
    
    super.onStart()
  }
  
  ** Migrates every instance of a given tag on a site from one name to another. That is, migrateTagVal("tag1","tag2") will find every instance of tag1 on a site, and
  ** change it to tag2, while maintaining the same value.
  private static Void migrateTagVal(Str tagFrom, Str tagTo, Proj proj){
    Grid tagFromRecs := proj.readAll(tagFrom)
    Diff[] diffs := [,]
    Bool updateLocalFuncs := false
    tagFromRecs.each(|Dict tagFromRec|{
      [Str:Obj?] changes := [:].set(tagFrom, Remove.val).set(tagTo, tagFromRec[tagFrom]) // For some reason we can't declare it directly here.
      diffs.add(Diff.make(tagFromRec, changes))
    })
    if(!diffs.isEmpty()) {
      updateLocalFuncs = true
      proj.commitAll(diffs)
      proj.log().info(diffs.size.toStr()+" records were converted from using "+tagFrom+" to "+tagTo)
    }
    
    if(updateLocalFuncs) { // If we do change over records, look at all funcs and adjust any 'src' tags that contain the old tag.
      Grid funcs := proj.readAll("func and src")
      Diff[] funcDiffs := [,]
      funcs.each(|Dict func|{
        Str oldSrc := func["src"]
        Str newSrc := oldSrc.replace(tagFrom, tagTo)
        funcDiffs.add(Diff.make(func, ["src": newSrc]))
      })
      if(!funcDiffs.isEmpty()) proj.commitAll(funcDiffs)
    }
  }
}