using haystack
using connExt
using hisExt

**
** bfitSmapConn
**
class BfitSmapConn : Conn
{
  new make(ConnActor actor, Dict rec) : super(actor, rec) {}

  override Obj? receive(ConnMsg msg) { return super.receive(msg) }
  
  ** When opened, pings the uri with the api key, and errors if it cannot connect.
  override Void onOpen() {
    onPing()
  }

  ** When closed, do nothing.
  override Void onClose() {}
  
  ** Ping the server, and return a dict indicating if the connection was successful. Otherwise, throw err.
  override Dict onPing() {
    Uri path := this.rec["uri"]
    Str key := this.proj().passwords().get(this.rec.id.toStr)
    BfitSmapUtils.ping(path, key)
    return Etc.makeDict([:])
  }
  
  override Void onPoll(){
    onSyncCur(pointsWatched)
  }
  
  override Obj? onSyncHis(ConnPoint point, Span dates){
    tickDates := [dates.start.ticks, dates.end.ticks]
    
    Obj[][] data := BfitSmapUtils.getData(this, point.rec, tickDates)
    HisItem[] his := data.map|Obj[] value -> HisItem|{
      val := value.last
      tz := point.tz
      ts := DateTime.makeTicks(value.first, tz)
      if(point.rec.get("kind") == "Number") val = Number.make(val)
      return HisItem(ts, val)
    }
    return point.updateHisOk(his, dates)
  }
  
  override Grid onLearn(Obj? arg) {
    return BfitSmapLib.bfitSmapLearnEx(this, this.rec, arg)
  }
  
  ** Finds most recent point in last day, otherwise errors
  override Void onSyncCur(ConnPoint[] points){
    //Format dates to be start and stop ticks 
    tickDates := [(DateTime.now - 1day).ticks, DateTime.now.ticks]
    
    points.each|ConnPoint point|{
      Obj[][] data := [,]
      try{ 
        data = BfitSmapUtils.getData(this, point.rec, tickDates)
        if(data.isEmpty || data.last == null) {point.updateCurErr(Err("No fresh data"))}
        else{
          Obj[] value := data.last
          val := value.last
          if(point.rec.get("kind") == "Number") val = Number.make(val)
          point.updateCurOk(val)
        }
      } catch(Err err) {point.updateCurErr(err)}
    }
  }
  
  override Duration? pollFreq(){
    /*
      try{
        return Duration.fromStr(rec.get("pollFreq").toStr, false) ?: 10min
      }catch (Err e) log.info("Poll Freq", e)
    */
    
    //too many requests returns a 429 error,
    return 300sec
  }
  
}
