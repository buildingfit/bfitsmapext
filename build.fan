#! /usr/bin/env fan

using build
using stackhub

**
** Build: bfitSmapExt
**
class Build : BuildPod
{
  @Target { help = "Publish to stackhub.org " }
  Void publish() {
    stackhub::PublishTask(this).run
  }
  
  new make()
  {
    podName = "bfitSmapExt"
    summary = "sMAP Connector"
    version = Version("2.0.2")
    meta    = [
      "org.name": "BuildingFit",
      "license.name": "MIT",
    ]
    depends  = [
      "axon 3.0",
      "connExt 3.0.22+", // Needed for Conn 3.0 interface
      "folio 3.0",
      "fresco 3.0",
      "haystack 3.0",
      "hisExt 3.0",
      "skyarcd 3.0",
      "sys 1.0",
      "util 1.0",
      "web 1.0",
      "classicConnExt 1.0+"
    ]
    srcDirs = [`fan/`]
    resDirs = [`locale/`, `res/`, `res/img/`, `lib/`]
    index   =
    [
      "skyarc.ext": "bfitSmapExt::BfitSmapExt",
      "skyarc.lib": "bfitSmapExt::BfitSmapLib"
    ]
  }
}